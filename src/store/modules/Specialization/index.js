/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';
import router from '../../../router';


const state = {
    SpecSkills: [],
}

// getters
const getters = {
    getSpecSkills: state => {
        return state.SpecSkills;
    },
}

// actions
const actions = {
    get_skills(context, id) {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        data.append("id_spec", id)
        api 
        .post("admin/get_skills", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit('set_spec_skills', response.data.skills);
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 
            console.log("error" + error); 
        });
    },
}

// mutations
const mutations = {
    
    set_spec_skills(state, skills) {
        state.SpecSkills = skills
    },
    
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
