/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';
import router from '../../../router';
import Nprogress from 'nprogress';


const state = {
    courses: [],
    potoks: [],
    specs: []
}

// getters
const getters = {

    getCourses: state => {
        return state.courses;
    },
    getSpecs: state =>{
        return state.specs;
    },
    getPotoks: state =>{
        return state.potoks
    }
}

// actions
const actions = {
    get_courses(context, id) 
    {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        api 
        .post("admin/get_courses", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_courses", response.data.courses)
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 
            console.log("error" + error); 
        });

    },
    get_specs(context)
        {
            const data = new FormData()
            data.append("token", store.getters.getUser)
            api 
            .post("admin/get_specs", data) 
            .then(response => { 
                    if(response.data.error == 0)
                    {
                        context.commit("set_specs", response.data.potoks)
                    }
                    if(response.data.error == 4)
                    {
                        store.dispatch("logout");
                    }
                    if(response.data.error == 5)
                    {
                        router.push("/");
                    }
                }) 
                .catch(error => { 
                
            });

        },
    get_potoks(context)
    {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        api 
        .post("admin/get_potoks", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_potoks", response.data.potoks)
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 
            console.log("error" + error); 
        });

    },

    
}

// mutations
const mutations = {
    
    set_courses(state, courses) {
        state.courses = courses;
    },
    set_potoks(state, potoks) {
        state.potoks = potoks;
    },
    set_specs(state, specs) {
        state.specs = specs;
    },
    
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
