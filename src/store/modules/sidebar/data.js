// Sidebar Routers
export const menus = {
	
	'': [
		
		{
			action: 'zmdi-accounts',
			title: 'Ученики',
			active: false,
			role: [2],
			label: 'Old',
			items: [
				
				{ title: 'Все', path: '/students/all',label: 'Old', exact: true, },
				{ title: 'Добавить', path: '/student/add',label: 'Old', exact: true, },
			]
		},
		{
			action: 'zmdi-wrench',
			title: 'Настройки',
			active: false,
			role: [2],
			label: 'Old',
			items: [
				{ title: 'Потоки', path: '/potoks',label: 'Old', exact: true, },
				{ title: 'Администраторы', path: '/admins',label: 'Old', exact: true, },
				{ title: 'Направления', path: '/specializations',label: 'Old', exact: true },
			]
		},
		{
			action: 'zmdi-book',
			title: 'Курсы',
			active: false,
			items: null,
			role: [2],
			path: '/courses',
			label: 'Old',
			exact: true
		},
		{
			action: 'zmdi-book',
			title: 'Проекты',
			active: false,
			items: null,
			role: [2],
			path: '/projects',
			label: 'Old',
			exact: true
		},
		
	],
	
}
