/**
 * Sidebar Module
 */

import { menus } from './data.js';
import { store } from '../../store';
import API from 'Api'

const state = {
    menus,
    teacher_info: {
        teacher_spec_admin_info: [],
        teacher_specs_info: [],
        teacher_courses_info: [],
        teacher_projects_info: [],
    }
}

const getters = {
    menus: state => {
        return state.menus;
    },
    getTeacherSpecAdmin: state => {
        return state.teacher_info;
    },
}

const actions = {
    setActiveMenuGroup(context, payload) {
        context.commit('setActiveMenuGroupHandler', payload);
    },
    get_teachers_specs(context){
        const data = new FormData();
        data.append('token', store.getters.getUser);
        API
            .post('admin/get_teachers_specs', data)
            .then(response => {
                if (response.data.error == 0){
                    context.commit('set_teachers_spec_admin', response);
                }else if (response.data.error == 4){
                    store.dispatch('logout');
                }
            })
    },
}

const mutations = {
    setActiveMenuGroupHandler(state, router) {
        let fullPath = '';
        if(router.pathURL){
            fullPath = router.pathURL;
        }else{
            fullPath = router.history.current.fullPath;
        }
        let path = fullPath.split('/');
        let matchedPath = '/' + path[2] + '/' + path[3];
        for (const category in state.menus) {
            for(const menuGroup in state.menus[category]) {
                if(state.menus[category][menuGroup].items !== null) {
                    for(const matched in state.menus[category][menuGroup].items){
                        if(state.menus[category][menuGroup].items[matched].path == matchedPath || state.menus[category][menuGroup].items[matched].path == fullPath ){
                           state.menus[category][menuGroup].active = true;
                        }
                    }
                }
            }
        }
    },
    set_teachers_spec_admin(state, spec_info){
        state.teacher_info.teacher_spec_admin_info = spec_info.data.data.spec_admin;
        state.teacher_info.teacher_specs_info = spec_info.data.data.specs;
        state.teacher_info.teacher_projects_info = spec_info.data.data.projects;
        state.teacher_info.teacher_courses_info = spec_info.data.data.courses;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}