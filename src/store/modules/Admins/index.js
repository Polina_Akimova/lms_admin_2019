/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';


const state = {
    admins: [],
}

// getters
const getters = {
    getAdmins: state => {
        return state.admins;
    },

}

// actions
const actions = {
    get_admins(context, token) {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        api 
        .post("admin/get_admins", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_admins", response.data.potoks);
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    this.$router.push("/");
                }
            }) 
            .catch(error => { 
            console.log("error" + error); 
        });
        
        
    },
}

// mutations
const mutations = {
    
    set_admins(state, admins) {
        state.admins = admins;
    },
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
