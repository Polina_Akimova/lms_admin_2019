/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';
import router from '../../../router';


const state = {
    projects: [],
}

// getters
const getters = {
    getProjects: state => {
        return state.projects;
    },
}

// actions
const actions = {
    get_projects(context) 
    {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        api 
        .post("admin/get_projects", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit('set_projects',response.data.projects)
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    router.push("/");
                }
            }) 
            .catch(error => { 
            console.log("error" + error); 
        });

    },
}

// mutations
const mutations = {
    
    set_projects(state, projects) {
        state.projects = projects
    },
    
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
