/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';
import router from '../../../router';


const state = {
    project: {},
    projectStudents: [],
    projectAsks: [],
    projectSpecs: []
}

// getters
const getters = {

    getProject: state => {
        return state.project;
    },
    getProjectStudents: state =>{
        return state.projectStudents
    },
    getprojectAsks: state =>{
        return state.projectAsks
    },
    getprojectSpecs: state => {
        return state.projectSpecs
    },
}

// actions
const actions = {
    get_project(context, id) {
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_project', id);
        api
        .post('admin/project_info', data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_project", response.data.project[0]);
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 

        });
    },
}

// mutations
const mutations = {
    
    set_project(state, project) {
        state.project = project
        state.projectStudents = project.students;
        state.projectAsks = project.asks;
        state.projectSpecs = project.specs;
    },
    
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
