/**
 * Auth Module
 */
import Vue from 'vue'

import Nprogress from 'nprogress';
import router from '../../../router';
import api from "Api";


const state = {
    user: localStorage.getItem('user'),
    role: localStorage.getItem('role')
}

// getters
const getters = {
    getUser: state => {
        return state.user;
    },
    getRole: state => {
        return state.role;
    }

}

// actions
const actions = {
    signinUser(context, user) {
        context.commit('loginUser');
        const data = new FormData()
        data.append("mail", user.email)
        data.append("pass", user.password)
       
        api 
           .post("admin/login", data) 
           .then(response => {
                if(response.data.error == 0)
                {

                    context.commit('loginUserSuccess', {token: response.data.token, role: response.data.role});
                }else if (response.data.error == 3){
                    context.commit('loginUserFailure', {message: 'Неправильные данные'});
                }
            }) 
            .catch(error => { 
               console.log("error" + error); 
         });
        
        
    },
    logout(context)
    {
        context.commit('logoutUser');
    }
}

// mutations
const mutations = {
    loginUser(state) {
        Nprogress.start();
    },
    loginUserSuccess(state, user) {
        state.user = user.token;
        state.role = user.role;
        localStorage.setItem('user', user.token);
        localStorage.setItem('role', user.role);
        router.push("/");
        setTimeout(function(){
            Vue.notify({
                group: 'loggedIn',
                type: 'success',
                text: 'Успешный вход!'
            });
       },1500);
    },
    loginUserFailure(state, error) {
        Nprogress.done();
        Vue.notify({
            group: 'loggedIn',
            type: 'error',
            text: error.message
        });
    },
    logoutUser(state) {
        state.user = null
        localStorage.removeItem('user');
        router.push("/session/login");
    },
    signUpUser(state) {
        Nprogress.start();
    },
    signUpUserSuccess(state, user) {
        state.user = localStorage.setItem('user', user);
        router.push("/default/dashboard/ecommerce");
        Vue.notify({
            group: 'loggedIn',
            type: 'success',
            text: 'Account Created!'
        });
    },
    signUpUserFailure(state, error) {
        Nprogress.done();
         Vue.notify({
            group: 'loggedIn',
            type: 'error',
            text: error.message
        });
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
