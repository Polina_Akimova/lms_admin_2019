/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';
import router from '../../../router';
import Nprogress from 'nprogress';


const state = {
    lessons: [],
    logs: [],
    asks: [],
    skills: [],
    homeworks:[]
}

// getters
const getters = {

    getLessons: state => {
        return state.lessons;
    },
    getHomeWorks: state =>{
        return state.homeworks
    },
    getSkills: state =>{
        return state.skills
    },
    getAsks: state => {
        return state.asks
    },
    getLogs: state => {
        return state.logs;
    }
}

// actions
const actions = {
    get_asks(context, id) {
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_course', id);
        api 
        .post("admin/get_course_asks", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_asks", response.data.asks)
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 

        });
    },
    get_skills(context, id) {
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_course', id);
        api 
        .post("admin/get_course_skills", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("get_skills", response.data.skills)

                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                   $router.push("/");
                }
            }) 
            .catch(error => { 

        });
    },
    get_homeworks(context, id){
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_course', id);
        api
        .post('admin/get_course_homeworks', data)
        .then(response => {
            if (response.data.error == 0){
                context.commit("set_homeworks", response.data.homeworks)
            }else if (response.data.error == 4){
                store.dispatch("logout");

            }
        })
    },
    get_logs(context, id) {
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_course', id);
        api
        .post('admin/get_course_logs', data)
        .then(response => {
            if (response.data.error == 0){
                context.commit("set_logs", response.data.logs)
            }else if (response.data.error == 4){
                store.dispatch("logout");

            }
        })
    },
    get_lessons(context, id) {
        Nprogress.start()
        const data = new FormData()
        data.append("token", store.getters.getUser)
        data.append("id", id)
        api 
        .post("admin/get_lessons", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit('set_lessons', response.data.lessons)
                    Nprogress.done()
                    
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 

        });
    },
}

// mutations
const mutations = {
    
    set_lessons(state, lessons) {
        state.lessons = lessons;
    },
    set_logs(state, logs) {
        state.logs = logs;
    },
    set_homeworks(state, homeworks) {
        state.homeworks = homeworks;
    },
    set_asks(state, asks) {
        state.asks = asks;
    },
    get_skills(state, skills){
        state.skills = skills;
    }
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
