/**
 * Auth Module
 */
import api from "Api";
import { store } from '../../store';


const state = {
    school_courses: [],
    potok_students: [],
    teachers: []
}

// getters
const getters = {
    getSchoolCourses: state => {
        return state.school_courses;
    },
    getPotokStudents: state => {
        return state.potok_students;
    },
    getTeachers: state => {
        return state.teachers;
    }

}

// actions
const actions = {
    get_teachers(context)
    {
        const data = new FormData()
        data.append("token", store.getters.getUser)
        api 
        .post("admin/get_teachers", data) 
        .then(response => { 
                if(response.data.error == 0)
                {
                    context.commit("set_teachers", response.data.teachers)
                }
                if(response.data.error == 4)
                {
                    store.dispatch("logout");
                }
                if(response.data.error == 5)
                {
                    
                    router.push("/");
                }
            }) 
            .catch(error => { 
    
        });

    },
    get_school_courses(context, id){
        const data = new FormData()
        data.append("token", store.getters.getUser)
        data.append("id", id)
        api 
        .post("admin/get_courses_in_school", data) 
        .then(response => { 
            if(response.data.error == 0)
            {    
                context.commit("set_school_courses", response.data.info)
            }
            if(response.data.error == 4)
            {
                store.dispatch("logout");
            }
            if(response.data.error == 5)
            {
                router.push("/");
            }
        }) 
        .catch(error => { 
        
        });

    },
    get_potok_students(context, id){
        const data = new FormData();
        data.append('token', store.getters.getUser);
        data.append('id_potok', id);
        api
        .post('admin/get_potok_students', data)
        .then(response => {
            if (response.data.error == 0){
                context.commit("set_potok_students", response.data.students)
            }else if (response.data.error == 4){
                store.dispatch("logout");

            }else if (response.data.error == 5){
                router.push("/");

            }
        })
        
    },
}

// mutations
const mutations = {
    
    set_school_courses(state, school_courses) {
        state.school_courses = school_courses;
    },
    set_potok_students(state, students) {
        state.potok_students = students;
    },
    set_teachers(state, teachers) {
        state.teachers = teachers;
    }
    
}

export default {
    state,
    getters,
    actions,
    mutations
}
