import Vue from 'vue'
import Vuex from 'vuex'

// modules
import auth from './modules/auth';
import chat from './modules/chat';
import settings from './modules/settings';
import ecommerce from './modules/ecommerce';
import mail from './modules/mail';
import sidebar from './modules/sidebar';
import admins from './modules/Admins';
import course from './modules/course';
import courses from './modules/courses';
import potok from './modules/potok';
import project from './modules/project';
import projects from './modules/projects';
import specialization from './modules/Specialization';
Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        auth,
        project,
        // chat,
        settings,
        specialization,
        // ecommerce,
        projects,
        // mail,
        sidebar,
        potok,
        admins,
        course,
        courses
    }
})
