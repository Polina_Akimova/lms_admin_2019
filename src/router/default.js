import Full from 'Container/Full'



//мое
const Potoks = () => import('Views/my/Potoks');
const Potok = () => import('Views/my/Potok');
const Admins = () => import('Views/my/Admins');
const AddStudent = () => import('Views/my/AddStudent');
const Student = () => import('Views/my/Student');
const Students = () => import('Views/my/Students');
const Specializations = () => import('Views/my/Specializations');
const Specialization = () => import('Views/my/Specialization');
const School_Course = () => import('Views/my/School_Course');
const Courses = () => import('Views/my/Courses');
const Course = () => import('Views/my/Course');
const Homework_check = () => import('Views/my/Homework_check');
const Homework_course = () => import('Views/my/Homework_course');
const Projects = () => import("Views/my/Projects");
const Project = () => import('Views/my/Project');
export default {
   path: '/',
   component: Full,
   redirect: '/potoks',
   // redirect: to => {
   //     if (this.$store.getters.getRole == 1){
   //         if(this.$store.getters.getTeacherSpecAdmin.teacher_spec_admin_info.length > 0){
   //             return `/specialization/${this.$store.getters.getTeacherSpecAdmin.teacher_spec_admin_info[0].id}`;
   //         }
   //         else
   //             return `/project/${this.$store.getters.getTeacherSpecAdmin.teacher_projects_info[0].id_project}`;
   //     }
   //         return '/potoks';
   // },
   children: [
      {
        path: '/potoks',
        component: Potoks,
        meta: {
           requiresAuth: true,
           title: '',
           breadcrumb: null
        }
     },
     {
      path: '/admins',
      component: Admins,
      meta: {
         requiresAuth: true,
         title: '',
         breadcrumb: null
      }
    },
    {
      path: '/specializations',
      component: Specializations,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/specialization/:id',
      component: Specialization,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/potok/:id',
      component: Potok,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/student/profile/:id',
      component: Student,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/student/add',
      component: AddStudent,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/students/all',
      component: Students,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/potok/:id/course/:id_course',
      component: School_Course,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/potok/:id/course/:id_course/homework/:id_homework',
      component: Homework_check,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/courses',
      component: Courses,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/course/:id',
      component: Course,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/course/:id_course/homework/:id',
      component: Homework_course,
      meta: {
          requiresAuth: true,
          title: '',
          breadcrumb: null
      }
  
    },
    {
      path: '/projects',
      component: Projects,
      meta: {
        requiresAuth: true,
        title: '',
        breadcrumb: null
      }
    },
    {
      path: '/project/:id',
      component: Project,
      meta: {
        requiresAuth: true,
        title: '',
        breadcrumb: null
      }
    },
   ]
}
